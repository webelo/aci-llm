from abc import ABC, abstractmethod
from datasets import dataset_factory
from tqdm import tqdm
import guidance


class BaseTask(ABC):
    """
    BaseTask is an abstract base class designed to be a blueprint for any task. It provides
    a structured way to define, run, and evaluate a task on a specific dataset.

    Attributes:
        dataset: A loaded dataset object returned from the dataset_factory.
        results: A dict to store the results of the task, including dataset_id,
                 prompt, log (list of evaluation for each example), and metrics (aggregate evaluation).
        prompt: The prompt used by the task (str or object which will be passed onto _prompt_builder).
        model: An instance of the model that will be built using the '_prompt_builder' method.
    """
    def __init__(self, dataset_id, prompt):
        """
        Initializes the task with a dataset and prompt.

        Args:
            dataset_id: Identifier of the dataset.
            prompt: The prompt to use for the task.
        """
        self.dataset = dataset_factory(dataset_identifier=dataset_id)
        self.results = {
            "dataset_id": dataset_id,
            "prompt": prompt,
            "log": [],
            "metrics": {},
        }
        self.prompt = prompt
        self.model = self._prompt_builder()

    @abstractmethod
    def _prompt_builder(self):
        """
        Constructs the model instance for the task.
        The structure and parameters of the model depend on the specific task.

        Returns:
            An instance of the model.
        """
        pass

    @abstractmethod
    def process_example(self, example):
        """
        Transforms a raw example from the dataset into a format suitable for the model.

        Args:
            example: A single example from the dataset (type and structure can vary based on the dataset).

        Returns:
            processed_example: A dict of necessary parameters required by the model.
        """
        pass

    @abstractmethod
    def metric(self, sample, prediction):
        """
        Computes one or more metrics given a sample from the dataset and the model's prediction on it.
        The specifics of the metric computation depend on the task.

        Args:
            sample: A single example from the dataset.
            prediction: Model's prediction on the sample.

        Returns:
            A metric value or a dictionary of metric values. The structure can vary
            based on the specific metrics used in the task. Each metric value can be
            a numerical value (like float or int) or any other data type, depending
            on the specific metric.
        """
        pass

    @abstractmethod
    def aggregate(self, metrics):
        """
        Aggregates the metrics computed for each example.

        Depending on the task, this could be a simple aggregation (like computing
        the mean) or something more complex that involves multiple metrics.

        Args:
            metrics: List of metric values, each associated with a single example
                    in the dataset. Each metric value could be a simple value or a
                    dict of multiple metric values.

        Returns:
            An aggregate metric value, which could be a single value (like float
            or int), a dict, a tuple or any other structure, depending on the
            complexity of the metrics.
        """
        pass

    @abstractmethod
    def log(self, sample, prediction):
        """
        Creates a log entry for a single sample and its prediction.
        The log entry typically includes information that will be useful for analysis and evaluation.

        Args:
            sample: A single example from the dataset.
            prediction: Model's prediction on the sample.

        Returns:
            A dictionary containing the log information for the sample and prediction.
        """
        pass

    def aggregate_results(self):
        """
        Extracts the metric values from the log entries in the results and
        aggregates them using the 'aggregate' method.
        Stores the aggregate metric in the 'metrics' field of the results dict.
        """
        metrics = [log['metric'] for log in self.results['log']]
        self.results['metrics'] = self.aggregate(metrics)

    def __call__(self):
        """
        Processes the entire dataset, makes predictions using the model, logs the predictions and associated metrics,
        and finally aggregates the metrics.
        The processed dataset, log entries, and aggregated metrics are stored in the 'results' dict.

        Returns:
            The 'results' dictionary with the dataset_id, prompt, log entries, and aggregated metrics.
        """
        for sample in tqdm(self.dataset): # TODO: async gather
            processed_example = self.process_example(sample)
            prediction = self.model(**processed_example)
            # import pdb; pdb.set_trace()
            log = self.log(sample, prediction)
            if 'metric' not in log:
                log['metric'] = self.metric(sample, prediction)
            self.results['log'].append(log)
        self.aggregate_results()
        return self.results



class MCQAZeroShot(BaseTask):
    prompt_str = '''{{header}}{{#each ctx}}{{pre}}{{this.question}}{{mid}}\n{{option_func this.options}}\n{{post}}{{this.answer}}{{sep}}\n{{/each~}}{{pre}}{{sample.question}}{{mid}}\n{{option_func sample.options}}\n{{post}}{{select 'answer' logprobs='logprobs' options=option_ids}}'''
    chat_prompt_str = '''{{#user}}{{header}}{{#each ctx}}{{pre}}{{this.question}}{{mid}}\n{{option_func this.options}}\n{{post}}{{this.answer}}{{sep}}\n{{/each~}}{{pre}}{{sample.question}}{{mid}}\n{{option_func sample.options}}\n{{post}}{{/user}}{{#assistant}}{{gen 'answer' stop="." max_tokens=1}}{{/assistant}}'''
    def __init__(self, dataset_id, prompt):
        super().__init__(dataset_id, prompt)
        self.presets = {**prompt, "option_func": self.process_options}

    def process_options(self, options):
        sep = ". "
        stringBuilder = []
        for k,v in options.items():
                stringBuilder.append("".join([str(k), sep, str(v)]))
        return "\n".join(stringBuilder)

    def _prompt_builder(self):
        if guidance.llm.chat_mode:
            model = guidance(self.chat_prompt_str)
        else:
            model = guidance(self.prompt_str)
        return model

    def process_example(self, example):
        return {**self.presets, "sample": example, "option_ids":list(example['options'].keys()), "ctx": []}

    def metric(self, sample, prediction):
        return int(sample['answer'].lower() == prediction['answer'].lower())

    def aggregate(self, metrics):
        return sum(metrics) / len(metrics)

    def log(self, sample, prediction):
        log = {
            "id": sample["id"],
            "output": str(prediction),
            "answer": sample['answer'],
            "predicted_answer": prediction['answer'],
            "metric": self.metric(sample, prediction),
        }
        if not guidance.llm.chat_mode:
            log["logprobs"] = prediction["logprobs"],
        return log


class MCQAFewShot(BaseTask):
    prompt_str = '''{{header}}{{#each ctx}}{{pre}}{{this.question}}{{mid}}\n{{option_func this.options}}\n{{post}}{{this.answer}}{{sep}}\n{{/each~}}{{pre}}{{sample.question}}{{mid}}\n{{option_func sample.options}}\n{{post}}{{select 'answer' logprobs='logprobs' options=option_ids}}'''
    chat_prompt_str = '''{{#user}}{{header}}{{#each ctx}}{{pre}}{{this.question}}{{mid}}\n{{option_func this.options}}\n{{post}}{{this.answer}}{{sep}}\n{{/each~}}{{pre}}{{sample.question}}{{mid}}\n{{option_func sample.options}}\n{{post}}{{/user}}{{#assistant}}{{gen 'answer' stop="." max_tokens=1}}{{/assistant}}'''
    def __init__(self, dataset_id, prompt, num_shots):
        self.presets = {**prompt, "option_func": self.process_options}
        self.num_shots = num_shots
        super().__init__(dataset_id, prompt)

    def process_options(self, options):
        sep = ". "
        stringBuilder = []
        for k,v in options.items():
                stringBuilder.append("".join([str(k), sep, str(v)]))
        return "\n".join(stringBuilder)

    def _prompt_builder(self):
        if guidance.llm.chat_mode:
            model = guidance(self.chat_prompt_str)
        else:
            model = guidance(self.prompt_str)
        return model

    def process_example(self, example):
        import random
        K = self.num_shots
        dataset_size = len(self.dataset)
        if dataset_size <= 1:
            raise ValueError("Dataset contains only one element, cannot select a different example.")

        example_id = example['id']
        # Generate K unique random indexes, excluding the index of the current example
        random_indexes = random.sample([i for i in range(dataset_size) if self.dataset[i]['id'] != example_id], min(K, dataset_size - 1))

        # Retrieve the few-shot examples from the dataset
        ctx = [self.dataset[i] for i in random_indexes]
        return {**self.presets, "sample": example, "ctx": ctx, "option_ids":list(example['options'].keys())}

    def metric(self, sample, prediction):
        return int(sample['answer'].lower() == prediction['answer'].lower())

    def aggregate(self, metrics):
        return sum(metrics) / len(metrics)

    def log(self, sample, prediction):
        log = {
            "id": sample["id"],
            "output": str(prediction),
            "answer": sample['answer'],
            "predicted_answer": prediction['answer'],
            "metric": self.metric(sample, prediction),
        }
        if not guidance.llm.chat_mode:
            log["logprobs"] = prediction["logprobs"],
        return log

