from .udemy import UdemyMCQA, UdemyOneshot, UdemyThreeshot, UdemyFiveshot
from .consolidated import ConsolidatedMCQA, ConsolidatedOneshot, ConsolidatedThreeshot, ConsolidatedFiveshot

TASK_MAP = {
    'udemy': UdemyMCQA,
    'udemy1': UdemyOneshot,
    'udemy3': UdemyThreeshot,
    'udemy5': UdemyFiveshot,
    'all': ConsolidatedMCQA,
    'all1': ConsolidatedOneshot,
    'all3': ConsolidatedThreeshot,
    'all5': ConsolidatedFiveshot,
}

def task_factory(task_identifier, **kwargs):
    task_class = TASK_MAP.get(task_identifier)
    if task_class is None:
        raise ValueError(f"Invalid dataset identifier {task_identifier}.")
    task_args = {k: v for k, v in kwargs.items() if k in task_class.__init__.__code__.co_varnames}
    return task_class(**task_args)
