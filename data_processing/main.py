CONSOLIDATED = "data_processing/datasets/consolidated_questions.jsonl"
TME_QUIZ_DIR = "data_processing/tme_quizzes/"
TME_RESULTS_PATH = "data_processing/tme_quizzes/results.json"

def json_loader(filename):
    """ 
    takes a filename and outputs the contents of a file
    Args:
        filename (string): PATH of the file
    Returns: 
        data (list): contents of the file
    """
    import json
    with open(filename, "r") as f:
        data = json.load(f)
    return data

def print_questions(filename):
    """ 
    takes a filename and neatly prints all the questions
    Args:
        filename (string): PATH of the file
    Returns: 
        prints the questions in MCQ style
    """
    jsonl = json_loader(filename)
    for js in jsonl:
        print(str(js["id"])+". "+js["question"])
        options = js["options"]
        for key, val in options.items():
            print(key+": "+val)
        print("\n")

def sample_questions(name, filename=CONSOLIDATED):
    """
    samples a subset of questions from all questions for a specific person
    Args:
        filename (string): the PATH of the questions that you want to be sampled from 
        defaults to CONSOLIDATED
        name (string): the name of the person who you are sampling the questions for
    Returns:
        a file is stored in the dir in the path
    """
    import random
    import os
    json = json_loader(filename)
    rand_list = [random.randint(0, len(json) - 1) for i in range(0,20)]
    os.makedirs(TME_QUIZ_DIR + name)
    questions = open(TME_QUIZ_DIR + name + "/questions.txt", 'w')
    ans = []
    for obj in json:
        if obj["id"] in rand_list:
            ans.append(obj["answer"])
            questions.write(obj["question"] + "\n")
            options = obj["options"]
            for key, val in options.items():
                questions.write(key+": "+val+"\n")
            questions.write("\n")
    questions.close()
    open(TME_QUIZ_DIR + name + "/corr_answers.txt", 'w').write(str(ans))

def read_txt_contents(path):
    """
    takes two paths as evaluates them into a python obj
    ARGS:
        path1 (string): PATH of file 
    Returns:
        key (list): the contents of path as a python list 
    """
    with open(path, 'r') as file: 
        key = eval(file.read())
    return key

# def read(path1):
#     with open(path1, 'r') as file: 
#         key = eval(file.read())
#     return key

def answers_score(name):
    """
    computes the score of the responses
    ARGS:
        name (string): name of the user whose scores you are computing
    Returns:
        overall_score (float): the score of the individual
    """
    key = TME_QUIZ_DIR + name + "/ans.txt"
    response = TME_QUIZ_DIR + name + "/corr_answers.txt"
    key = read_txt_contents(key)
    response = read_txt_contents(response)
    score = 0
    for i in range(len(key)):
        if key[i] == response[i]:
            score += 1
    overall_score = (score/len(response))
    return overall_score

def label_exists(ls, val):
    """
    verifies if a value exists in a certain list of dictionaries
    ARGS:
        ls (list): the list of dictionaries that you are checking for
        val (string, float): the value you are looking for
    Returns:
        True if value is present, False o/w
    """
    for dict in ls:
        if dict["name"] == val:
            return True
    return False

def add_to_results(name):
    """
    adds the score of an individual to the json file where we are storing results
    ARGS:   
        name (string): name of the person that you are adding results of
    """
    import os
    import json
    existing_data = json_loader(TME_RESULTS_PATH)
    if not label_exists(existing_data, name):
        new_obj = {"name" : name, "score" : answers_score(name)}
        existing_data.append(new_obj)
    with open(TME_RESULTS_PATH, "w") as file:
        json.dump(existing_data, file, indent=4)

def refresh():
    """
    refreshes the tme quiz results, adding any scores that have not been inputed yet
    """
    import os
    for folder_name in os.listdir(TME_QUIZ_DIR):
        if os.path.isfile(TME_QUIZ_DIR + "/" + folder_name +"/ans.txt"):
            add_to_results(folder_name)

def compute_average():
    """
    computes the average of all scores in the results file
    Returns:
        double: the average percent of correct answers
    """
    refresh()
    import json
    existing_data = json_loader(TME_RESULTS_PATH)
    scores = [d["score"] for d in existing_data]
    return round(sum(scores)/len(scores), 3)

print(compute_average())