In an ACI mode deployment with a Layer 2 fabric, what is the maximum number of Spine switches that can be deployed?
a: 6 Spine switches per fabric
b: 12 Spine switches per fabric
c: 18 Spine switches per fabric
d: 24 Spine switches per fabric

In an ACI mode deployment with a Layer 3 fabric, what is the maximum number of Leaf switches that can be deployed?
a: 100 Leaf switches per fabric
b: 200 Leaf switches per fabric
c: 500 Leaf switches per fabric
d: 750 Leaf switches per fabric

In an ACI Spine-Leaf architecture, where are servers typically connected?
a: They are connected to Spine switches.
b: They are connected to Leaf switches.
c: They are connected to both Spine and Leaf switches.
d: They are not directly connected to switches in this architecture.

In an ACI fabric, can endpoints such as servers, firewalls, IDS, IPS, and bare metal servers communicate with each other by default?
a: Yes, they can communicate without any configuration.
b: No, communication between endpoints is not allowed by default.
c: It depends on the type of endpoints.
d: Only certain types of endpoints can communicate with each other.

Which of the following devices can be considered as endpoints?
a: Routers and switches
b: Virtual machines and bare-metal servers
c: Firewalls and IDS/IPS systems
d: All of the above

What is the definition of a 'Tenant' in Cisco ACI?
a: A secure and exclusive physical computing environment
b: A private network within the ACI fabric
c: A logical unit of isolation for policy management
d: An IP address assigned to a specific device

What services are typically handled within the MGMT Tenant in Cisco ACI?
a: Configuration of in-band and out-of-band connectivity for fabric nodes
b: Management of user-defined access policies for fabric nodes
c: Communication between front-panel interfaces and endpoints in the fabric
d: Provisioning of services for managing ACI fabric components

What is Interface Policy in ACI?
a: It is a policy used to configure VLAN settings on network interfaces.
b: It is a policy used to allocate bandwidth and prioritize traffic on network interfaces.
c: It is a policy used to define access control rules for network interfaces.
d: It is a policy used to enable and configure various protocols on network interfaces.

What is meant by Switch and Interface Profiles in ACI?
a: Switch Profiles are used to identify leaf switches in ACI, while Interface Profiles define the configuration for individual interfaces.
b: Switch Profiles define the configuration for individual leaf switches, while Interface Profiles are used to identify switch fabric in ACI.
c: Switch Profiles are used to configure individual interfaces, while Interface Profiles define the overall configuration for leaf switches in ACI.
d: Switch Profiles define the configuration for the entire ACI fabric, while Interface Profiles specify the configuration for the interfaces within the fabric.

What is meant by 'Contract' in Cisco ACI?
a: Contracts are used to permit or deny traffic flows within the ACI fabric and control traffic between EPGs.
b: Contracts define the source and destination IP addresses for communication within the ACI fabric.
c: Contracts determine the bandwidth allocation for different applications in the network.
d: Contracts establish virtual connections between leaf switches in the ACI fabric.

Which routing protocol is used for internal communication between ACI Spine and Leaf switches?
a: OSPF
b: EIGRP
c: RIP
d: MP-BGP

Which Cisco 9K models are commonly used as Spine Nodes in an ACI setup?
a: C9332C
b: C93180YC-FX3
c: C93120TX
d: All of the above

Can we connect Access Layer switches in the downlink to Leaf Nodes in an ACI setup?
a: Yes, we can connect Access Layer switches to Leaf Nodes in an ACI setup.
b: No, Access Layer switches cannot be connected to Leaf Nodes in an ACI setup.

You are using Cisco APIC GUI to troubleshoot Layer 3 connectivity within the Cisco ACI. Where can you inspect Layer 3 deny logs for the traffic flows?
a: Under the specific tenant, choose Operational > Flows > L3 Drop in the work pane.
b: Under the specific VRF, choose Operational > Flows > L3 Drop in the work pane.
c: Under the specific bridge domain, choose Operational > Flows > L3 Drop in the work pane.
d: Under the specific EPG, choose Operational > Flows > L3 Drop in the work pane.

Which two statements are correct regarding Cisco ACI Layer 2 unknown unicast packet forwarding? (Choose two.)
a: When the BD Layer 2 Unknown Unicast is set to Hardware Proxy mode and the MAC address of the destination endpoint is unknown to the spine COOP database (for example, silent host), the spine drops the packet.
b: When the BD Layer 2 Unknown Unicast is set to Hardware Proxy mode, Cisco ACI always drops Layer 2 unknown unicast packets.
c: When the BD Layer 2 Unknown Unicast is set to Hardware Proxy mode, the ingress leaf will encapsulate the Layer 2 unknown unicast packets using the destination leaf switch VTEP address, while sending it to the spine switches.
d: When the BD Layer 2 Unknown Unicast is set to Flood mode, the Layer 2 unknown unicast packets are flooded using the spine COOP database.
e: When the BD Layer 2 Unknown Unicast is set to Flood mode, the Layer 2 unknown unicast packets are flooded using one of the multicast trees rooted in the spine that is scoped to the BD.

Which three options are logical constructs of a typical Cisco ACI tenant network? (Choose three.)
a: STP domain
b: bridge domain
c: Layer 3 Out
d: Layer 4 Out
e: Layer 7 Out
f: VRF
g: ARP table

Which two statements regarding Cisco APIC cluster are correct? (Choose two.)
a: The Cisco APIC cluster sits in the traffic path.
b: The Cisco APIC cluster uses a large database technology called sharding.
c: A cluster size of 4 or 6 APICs is recommended.
d: The shards are distributed across two Cisco APICs for redundancy.
e: Any controller in the Cisco APIC cluster can service any user for any operation.

Which statement is correct regarding endpoint learning process in Cisco ACI?
a: The leaf switches store location and policy information only about endpoints that are directly attached to them.
b: The leaf switches store location and policy information only about endpoints that are directly attached to them or through a directly attached Layer 2 switch or Fabric Extender.
c: The leaf switches store location and policy information about endpoints that are attached directly to them (or through a directly attached Layer 2 switch or Fabric Extender), and endpoints that are attached to other leaf switches on the fabric.
d: The leaf switches do not store location and policy information about any endpoints, since it is stored in the Cisco APIC.

Which three options enable you to query or view the MIT in Cisco ACI fabric? (Choose three.)
a: Virtuale
b: Object Store Browser
c: Moquery
d: MITquery
e: Python SDK
f: Nova SDK

You are working as a consultant and you are hired to help a customer during Cisco ACI integration with Kubernetes environment. Since the customer is planning to use Red Hat OpenShift as a container application platform on top of Kubernetes, which plug-in will it need for the integration?
a: VMware vCenter plug-in
b: CNA plug-in
c: CNI plug-in
d: OpenStack plug-in

