In an ACI Spine-Leaf architecture, where are servers typically connected?
a: They are connected to Spine switches.
b: They are connected to Leaf switches.
c: They are connected to both Spine and Leaf switches.
d: They are not directly connected to switches in this architecture.

What is the purpose of a Tenant in Cisco ACI?
a: To provide a secure and exclusive virtual computing environment
b: To create private networks within the ACI fabric
c: To group policies and resources for a specific organization or domain
d: To assign unique IP addresses to endpoints

How many default tenants are there in Cisco ACI?
a: One: Common
b: Two: Common and Infra
c: Three: Common, Infra, and Management
d: Four: Common, Infra, Management, and User

What is the purpose of the Infra Tenant in Cisco ACI?
a: It handles configuration for internal fabric communication and functions
b: It is a separate tenant for managing user-related policies
c: It provides additional security measures for the ACI fabric
d: It allows communication between different ACI tenants

If an Interface Policy is not created in ACI, what will happen?
a: No policies will be applied to the interfaces.
b: The interfaces will automatically inherit the default policies.
c: The interfaces will be disabled and unable to transmit data.
d: The interfaces will experience performance issues due to the absence of policies.

Which Cisco 9K models are commonly used as Spine Nodes in an ACI setup?
a: 9316D-GX
b: 9332D-GX2B
c: 9336PQ
d: 9364D-GX2A
e: 9364C
f: All of the above

Which Cisco 9K models are commonly used as Spine Nodes in an ACI setup?
a: C9332C
b: C93180YC-FX3
c: C93120TX
d: All of the above

Which Cisco 9K models are commonly used as Leaf Nodes in an ACI setup?
a: C9316W-YMCA
b: C93600CD-GX
c: 9364C
d: All of the above

Can we integrate the management of third-party devices in the APIC controller?
a: Yes, the APIC controller allows integration and management of third-party devices.
b: No, the APIC controller does not support the management of third-party devices.

What is the concept of microsegmentation in Cisco ACI?
a: Microsegmentation refers to the process of dividing a physical network into smaller subnets for improved performance.
b: Microsegmentation involves the automatic assignment of endpoints to logical security zones called EPGs based on various attributes in Cisco ACI.
c: Microsegmentation is a feature in Cisco ACI that allows for the integration of third-party devices into the ACI fabric.
d: Microsegmentation is a technique used in Cisco ACI to enable seamless migration of virtual machines between different hypervisor platforms.

You are working as a network engineer and you are reviewing the VMware vCenter (VMM) integration with Cisco ACI. Which statement is correct?
a: EPGs are exposed to the VMM in a 1:n (as needed) mappings to port groups.
b: EPGs are exposed to the VMM in a 1:1 mapping to port groups.
c: EPGs are exposed to the VMM in a 1:1 mapping to VM networks.
d: EPGs are exposed to the VMM in a 1:n (as needed) mappings to VM networks.

Which two statements are correct regarding the Cisco ACI ARP gleaning feature? (Choose two.)
a: Cisco ACI uses ARP gleaning to send ARP requests to resolve the MAC address of an endpoint that is yet to be learned.
b: Cisco ACI uses ARP gleaning to send ARP requests to resolve the IP address of an endpoint that is yet to be learned.
c: ARP gleaning is triggered for (Layer 3) routed traffic regardless of configuration, such as ARP flooding, as long as the traffic is routed to an unknown IP.
d: When ARP gleaning is triggered, the fabric generates an ARP request that is originated from the management IP address of the ingress leaf switch.

Which three are logical constructs in the Cisco ACI policy model that are part of the tenant hierarchy? (Choose three.)
a: Bridge domain
b: VLAN pools
c: VRF
d: EPG
e: AAEP
f: Domain

What is the default syslog format in Cisco ACI?
a: RFC 5424
b: NX-OS style
c: IOS style
d: IOS XE style

Which option should you use to create a Port Channel interface in Cisco ACI?
a: switch policy
b: switch profile
c: interface policy group
d: interface profile

Which ASIC is used on second-generation Cisco Nexus 9300 Series switches?
a: NFE
b: ALE
c: LSE
d: UPC

Which three statements are correct for dynamic routing protocols support for Layer 3 connections in Cisco ACI? (Choose three.)
a: EIGRP (IPv4 and IPv6) is supported.
b: OSPFv2 (IPv4) and OSPFv3 (IPv6) are supported.
c: OSPFv2 (IPv4) is supported, while OSPFv3 (IPv6) is not supported.
d: BGP (IPv4 and IPv6) is supported.
e: IS-IS (IPv4 and IPv6) is supported.
f: IS-IS (IPv4) is supported, while IS-IS (IPv6) is not supported.
g: RIPv2 (IPv4) is supported, while RIPng (RIP for IPv6) is not supported.

What is the use of a device package in Cisco ACI?
a: external Layer 2 connection
b: external Layer 3 connection
c: Layer 4–Layer 7 service integration
d: Layer 5–Layer 7 service integration
e: inter-tenant communication

Which two are key Cisco ACI features? (Choose two.)
a: core-aggregation-access layer design
b: open APIs and a programmable SDN fabric
c: Fibre Channel support
d: manual IT workflows for better control
e: automation of IT workflows

