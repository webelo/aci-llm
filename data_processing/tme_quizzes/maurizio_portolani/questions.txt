How many APIC controllers are recommended in an ACI deployment of 150 switches?
a: Five
b: One
c: Three
d: Four

How many APIC controllers are recommended in an ACI deployment of 250 switches?
a: Seven
b: Two
c: Three
d: Five

What are some benefits of Cisco ACI compared to traditional network solution/architecture?
a: Centralized health monitoring of physical and virtual networks
b: Automation of repetitive tasks, reducing configuration errors
c: ACI is agnostic to both physical and virtual environments.
d: Elimination of flooding from the fabric
e: All of the above
f: None of the above

What is an End Point Group (EPG) in Cisco ACI?
a: A logical grouping of endpoints with similar characteristics
b: A physical device connected directly to the network
c: A virtual machine used for network testing
d: An IP address assigned to an endpoint

What is the purpose of a Tenant in Cisco ACI?
a: To provide a secure and exclusive virtual computing environment
b: To create private networks within the ACI fabric
c: To group policies and resources for a specific organization or domain
d: To assign unique IP addresses to endpoints

What is the purpose of the Infra Tenant in Cisco ACI?
a: It handles configuration for internal fabric communication and functions
b: It is a separate tenant for managing user-related policies
c: It provides additional security measures for the ACI fabric
d: It allows communication between different ACI tenants

What is meant by 'Contract' in Cisco ACI?
a: Contracts are used to permit or deny traffic flows within the ACI fabric and control traffic between EPGs.
b: Contracts define the source and destination IP addresses for communication within the ACI fabric.
c: Contracts determine the bandwidth allocation for different applications in the network.
d: Contracts establish virtual connections between leaf switches in the ACI fabric.

Can multiple Bridge Domains be created within the same VRF (Virtual Routing and Forwarding) in Cisco ACI?
a: No, only one Bridge Domain can be created within a VRF.
b: Yes, multiple Bridge Domains can be created within the same VRF.
c: Bridge Domains cannot be created within a VRF.
d: VRFs and Bridge Domains are not related.

Can we connect Access Layer switches in the downlink to Leaf Nodes in an ACI setup?
a: Yes, we can connect Access Layer switches to Leaf Nodes in an ACI setup.
b: No, Access Layer switches cannot be connected to Leaf Nodes in an ACI setup.

Your company's Cisco ACI uses an L3Out connection to the external network, which facilitates a routing exchange between the Cisco ACI and the external routers through OSPF. You have been assigned an incident ticket for an external network connectivity issue. You have verified that the required external routes are not learned by Cisco ACI. Which leaf switch CLI command can you use to verify the OSPF neighbor relationships between Cisco ACI and external routers?
a: show ip ospf neighbors vrf [vrf | all]
b: show ip ospf neighbors bd [bd | all]
c: show ip ospf neighbors l3out [l3out | all]
d: show ip ospf neighbors ospf_out

Which two statements are correct regarding the Cisco APIC cluster? (Choose two.)
a: Cisco APICs form a cluster and talk to each other using the out-of-band management network.
b: In a two Cisco APIC cluster, you will not be able to perform write operations if one of the controllers fails.
c: Cisco APIC cluster cannot have a standby Cisco APIC.
d: Standby Cisco APIC functionality enables you to operate one or more Cisco APICs in the standby mode.
e: Standby Cisco APIC functionality is not supported in a Cisco ACI Multi-Pod deployment.

Which option should you use to create a Port Channel interface in Cisco ACI?
a: switch policy
b: switch profile
c: interface policy group
d: interface profile

How does bridge domain handle different types of traffic in Cisco ACI?
a: By default, Layer 2 unknown unicast traffic is flooded.
b: By default, ARP flooding is enabled.
c: Unicast routing is disabled by default.
d: Hardware proxy, and Layer 2 unknown unicast and ARP flooding are two opposite modes of operation.

You are exploring the Cisco ACI faults state in the Cisco APIC GUI, so you can understand their life cycle. Which two statements are correct? (Choose two.)
a: The default soaking interval is 180 seconds.
b: The default soaking interval is 300 seconds.
c: After the soak time, the fault transitions to a raised state.
d: If the fault condition reoccurs during the retention interval, a new fault MO is created in the soaking state.
e: A fault raised by the system can have only one severity during its life cycle.

You are creating a bridge domain in Cisco ACI, so you can deploy a gateway on any leaf switches with EPGs associated to this bridge domain. Which two statements are correct? (Choose two.)
a: Bridge domain can belongs to multiple VRFs.
b: VRF can have one or more bridge domains.
c: Bridge domain is the Layer 2 forwarding domain.
d: The default gateway for the endpoint can be associated with only one leaf switch.
e: Bridge domain does not support Layer 2 flood traffic.

What does an EPG represent?
a: application network profile
b: policy enforcement
c: broadcast domain
d: collection of similar hosts or servers

You are a network engineer responsible for the Cisco ACI in your company’s data center and you want to automate CLI tasks with shell scripts using SSH for logging in to the Cisco APIC. How can you implement a password-free SSH login?
a: Configure a dedicated user without a password.
b: Associate X.509 certificate with a dedicated user.
c: Associate SSH public key with a dedicated user.
d: Associate SSH private key with a dedicated user.

Your company is in a process of installing Cisco ACI fabric that should connect to the existing network infrastructure that utilizes STP. Which two statement are correct regarding Cisco ACI and STP interaction, including fabric reaction to BPDU TCNs? (Choose two.)
a: STP is run within the Cisco ACI fabric.
b: BPDUs are flooded within an EPG.
c: BPDUs are flooded within a bridge domain.
d: When BPDU TCN is received, the Cisco APIC updates the topology, as requested.
e: When BPDU TCN is received, the Cisco APIC flushes the MAC addresses for the corresponding EPG.
f: When BPDU TCN is received, the Cisco APIC flushes the MAC addresses for the corresponding bridge domain.

