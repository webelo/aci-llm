from abc import ABC, abstractmethod


class BaseDataset(ABC):
    """
    Abstract Base Class representing a dataset. The dataset is loaded into memory when it is used.

    The dataset can be iterated over with a for loop or accessed using an index.

    Subclasses must implement the 'load' method to load the data into the 'data' attribute.
    """
    def __init__(self):
        self.data = None

    @abstractmethod
    def load(self):
        pass

    def __iter__(self):
        if self.data is None:
            self.load()
        self._iter = iter(self.data)
        return self

    def __next__(self):
        try:
            return next(self._iter)
        except StopIteration:
            raise StopIteration

    def __getitem__(self, index):
        if self.data is None:
            self.load()
        return self.data[index]

    def __len__(self):
        if self.data is None:
            self.load()
        return len(self.data)