from .udemy_dataset import UdemyDataset
from .consolidated_dataset import ConsolidatedDataset

DATASET_MAP = {
    'udemy-mcqa': UdemyDataset,
    'consolidated': ConsolidatedDataset,
}

def dataset_factory(dataset_identifier):
    dataset_class = DATASET_MAP.get(dataset_identifier)
    if dataset_class is None:
        raise ValueError(f"Invalid dataset identifier {dataset_identifier}.")
    return dataset_class()
